#+title: Ways to store strings read from a file (newline separated)

* Places to hold the data

- Stack ::
  declared within a function without "static" qualifier or malloc
  - limited,
    my system by default ~8MB

  only if work would be done all inside the same function or on it's chidren since returning from it would remove all the stored data from the stack
- Data segment
  - initialized data/uninitialized data :: outside any function or with "static" qualifier(be it inside or outside a function)
  - heap :: malloc/calloc

* Data Design decisions

1. string size
   1. fixed (memory wasteful)
   2. variable(best) string size
2. array of pointer to strings?
   1. fixed size
   2. variable
3. contiguous strings
4. program section to hold the data
   1. stack
   2. non_heap data segment
   3. heap

3+(1.1) requires no 2: fixed strings contiguous in memory is can be accessed with a multidimensional array, no need to save an extra array of pointers.

** plausibility of a stack overflow
~8MB limit, an 8MB text file with ~100 chars/line == 100bytes/ line
has ~80K lines - it's big but possible.

Worse if we want to use an array of pointers to strings.
8MB array of pointers => 1M pointers => 1M lines quite big but..

either way it's a limited solution since the function cannot return without losing the data.

* TOPPICK(1.2 in heap)+(2.2 in heap)+3 The most elegant organization of data
Idea is the full file goes into a contigous array.
\n are replaced with \0
- beware if file ends without a \n then allocation size must be 1 byte bigger and we need to write a \0 there

Array of pointers we have these choices:
- use conservative estimate #lines
  _buffer_overflow prone_
- 1 pointer per character then release excess
  out of memory prone
- count #lines after having of text
  create array of poimter with exact size
  travel text finding the correct location and assigining them to array of pointers (CHOSEN)

#+begin_src C :results output :tangle src/toppick.c
  #include <stdio.h>
  #include <stdlib.h>

  int
  read_lines(char** *p_array, char *pathname) {
    FILE *f = fopen(pathname, "r");
    if (f == NULL)
      return 0;

    fseek(f, 0, SEEK_END); // go to EOF
    long size = ftell(f); // size of file in bytes == #chars
    // before seeking back we need to know if file ends with a newline
    // or a another character
    fseek(f, -1, SEEK_END);
    if (fgetc(f) != '\n')
      size++;

    fseek(f, 0, SEEK_SET); // back to start

    if (size == 0)
      return 0;

    char *strings  = malloc(size*sizeof(char));
    int n_strings = 1;

    for (char *p = strings; p < strings + size*sizeof(char); p++) {
      ,*p = fgetc(f);
      if (*p == '\n')
        ,*p = '\0';
      else if (*p == EOF)
        ,*p = '\0';

      if ( p > strings && *(p-1) == '\0')
        n_strings++;
      }

    ,*p_array = malloc(n_strings*sizeof(char*));

    // iterating *strings finding '\0' and saving the pointers to them
    char **q = *p_array;
    ,*q++ = strings;
    for (char *p = strings; p < strings + size*sizeof(char); p++)
      if (*(p-1) == '\0')
        ,*q = p, q++;

    fclose(f);

    return n_strings;
  }

  int
  main(int argc, char *argv[]) {
    char **arr;
    char *pathname = argc > 1 ? argv[1] : "default.txt";
    int n_lines = read_lines(&arr, pathname);

    if (n_lines == 0)
      return 1;

    // lets print them all
    printf("Number of lines: %d\n", n_lines);
    for (int k = 0; k < n_lines; k++) {
      printf("line %d\n", k+1);
      puts( *(arr+k) );
    }
    
    return 0;
  }
#+end_src

* RUNNER-UP(1.2 in heap)+(2.2 in heap) Tight strings / Tight *char array - possible overflow of char* array/out of memory

#+begin_src C :results output :tangle src/runnerup.c
  #include <stdio.h>
  #include <stdlib.h>
  #include <string.h>

  int
  main(void) {
    FILE *f = fopen("file.txt", "r");
    // malloc a big array, then we shed it off
    char **strings = malloc(1000*sizeof(char*));
    char buf[100];
    int index = 0;

    for (char **p = strings;
         p < strings+1000 && fgets(buf, 100, f);
         p++) {
      strings[index] = malloc( (1+strlen(buf))*sizeof(char));
      if (!strings[index]) return 1;

      strcpy( strings[index], buf);
      index++;
    }

    // now shrink strings size
    char **new_ptr = realloc(strings, index*sizeof(char*));
    if (new_ptr)
      strings = new_ptr;

    for (char* *p = strings; p < strings+index; p++)
      printf("%p %s", *p, *p);

    fclose(f);
    return 0;
  }
#+end_src

* (1.1)+3+(1 or 2) Multidimensional Array, contiguous, no arr of char*, non-heap
#+begin_src C :tangle src/1.c
  #include <stdio.h>

  #define MAX_STRING_SIZE 4
  #define MAX_NUMBER_STRINGS 2

  int
  main(void) {
    FILE *f = fopen("file.txt", "r");
    char strings[MAX_NUMBER_STRINGS][MAX_STRING_SIZE];

    for (char (*p)[MAX_STRING_SIZE] = strings;
         p < strings+MAX_NUMBER_STRINGS && fgets(*p, MAX_STRING_SIZE, f);
         p++)
      ; // as linhas ficaram com o \n


    for (int k = 0; k < MAX_NUMBER_STRINGS; k++)
      printf("%s", strings[k]);

    fclose(f);
    return 0;
  }

#+end_src

char[2][3] degenera para char (*)[3]
*( char (*)[3]) == char[3] que degenera para char*

to see types with compiler warnings
#+begin_src C :results output
  #include <stdio.h>

  #define MAX_STRING_SIZE 3
  #define MAX_NUMBER_STRINGS 2

  int
  main(void) {
    char strings[MAX_NUMBER_STRINGS][MAX_STRING_SIZE];

    int *p = strings;
    return 0;
  }

#+end_src

* (1.1)+3+3 Multdimensional Array, contiguous, no array of char*, heap
#+begin_src C :tangle src/2.c
  #include <stdio.h>
  #include <stdlib.h>

  #define MAX_STRING_SIZE 4
  #define MAX_NUMBER_STRINGS 2

  int
  main(void) {
    FILE *f = fopen("file.txt", "r");
    char (*strings_p)[MAX_NUMBER_STRINGS][MAX_STRING_SIZE] =
      malloc(MAX_NUMBER_STRINGS*MAX_STRING_SIZE*sizeof(char));
    //equivalently
    //calloc(MAX_NUMBER_STRINGS, MAX_STRING_SIZE*sizeof(char));

    for (char (*p)[MAX_STRING_SIZE] = *strings_p;
         p < *strings_p+MAX_NUMBER_STRINGS && fgets(*p, MAX_STRING_SIZE, f);
         p++)
      ; // as linhas ficaram com o \n


    for (int k = 0; k < MAX_NUMBER_STRINGS; k++)
      printf("%s", (*strings_p)[k]);

    fclose(f);
    return 0;
  }

#+end_src

* (1.2 in heap)+(2.1 in stack)

#+begin_src C :tangle src/3.c
  #include <stdio.h>
  #include <stdlib.h>
  #include <string.h>

  #define BUF_SIZE 4
  #define MAX_NUMBER_STRINGS 2

  int
  main(void) {
    FILE *f = fopen("file.txt", "r");

    char *strings[MAX_NUMBER_STRINGS],
      buf[BUF_SIZE];

    for (char* *p = strings;
         p < strings+MAX_NUMBER_STRINGS && fgets(buf, BUF_SIZE, f);
         p++) {
      ,*p = malloc( (1+strlen(buf))*sizeof(char));
      if (*p == NULL) return 1;
      strcpy(*p, buf);
    }

    for (char* *p = strings; p < strings+MAX_NUMBER_STRINGS; p++)
      printf("%s", *p);

    fclose(f);
    return 0;
  }

#+end_src

* (1.2 in heap)+(2.1 in heap)
see previous.

remove strings declaration and change it to:
#+begin_src C
  char **strings = malloc(MAX_NUMBER_STRINGS*sizeof(char*));
#+end_src

then any "strings" reference in the previous must become "*strings"

* (1.1)+(2.2)+3+(1) Multidimensional array + array char* wasting spac
#+begin_src C :results output :tangle src/4.c
  #include <stdio.h>

  int
  main(void) {
    FILE *f = fopen("file.txt", "r");
    char* strings[100], contiguous_array[100][50];

    int k; // so k is visible outside loop
    for (k = 0; k < 100 && fgets(contiguous_array[k], 50, f); k++) {
      strings[k] = contiguous_array[k];
    }


    for (char* *p = strings; p < strings+k; p++)
      printf("%p %s", *p, *p);

    fclose(f);
    return 0;
  }
#+end_src

