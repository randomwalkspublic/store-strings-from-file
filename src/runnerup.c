#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main(void) {
  FILE *f = fopen("file.txt", "r");
  // malloc a big array, then we shed it off
  char **strings = malloc(1000*sizeof(char*));
  char buf[100];
  int index = 0;

  for (char **p = strings;
       p < strings+1000 && fgets(buf, 100, f);
       p++) {
    strings[index] = malloc( (1+strlen(buf))*sizeof(char));
    if (!strings[index]) return 1;

    strcpy( strings[index], buf);
    index++;
  }

  // now shrink strings size
  char **new_ptr = realloc(strings, index*sizeof(char*));
  if (new_ptr)
    strings = new_ptr;

  for (char* *p = strings; p < strings+index; p++)
    printf("%p %s", *p, *p);

  fclose(f);
  return 0;
}
