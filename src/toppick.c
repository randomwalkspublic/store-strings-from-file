#include <stdio.h>
#include <stdlib.h>

int
read_lines(char** *p_array, char *pathname) {
  FILE *f = fopen(pathname, "r");
  if (f == NULL)
    return 0;

  fseek(f, 0, SEEK_END); // go to EOF
  long size = ftell(f); // size of file in bytes == #chars
  // before seeking back we need to know if file ends with a newline
  // or a another character
  fseek(f, -1, SEEK_END);
  if (fgetc(f) != '\n')
    size++;

  fseek(f, 0, SEEK_SET); // back to start

  if (size == 0)
    return 0;

  char *strings  = malloc(size*sizeof(char));
  int n_strings = 1;

  for (char *p = strings; p < strings + size*sizeof(char); p++) {
    *p = fgetc(f);
    if (*p == '\n')
      *p = '\0';
    else if (*p == EOF)
      *p = '\0';

    if ( p > strings && *(p-1) == '\0')
      n_strings++;
    }

  *p_array = malloc(n_strings*sizeof(char*));

  // iterating *strings finding '\0' and saving the pointers to them
  char **q = *p_array;
  *q++ = strings;
  for (char *p = strings; p < strings + size*sizeof(char); p++)
    if (*(p-1) == '\0')
      *q = p, q++;

  fclose(f);

  return n_strings;
}

int
main(int argc, char *argv[]) {
  char **arr;
  char *pathname = argc > 1 ? argv[1] : "default.txt";
  int n_lines = read_lines(&arr, pathname);

  if (n_lines == 0)
    return 1;

  // lets print them all
  printf("Number of lines: %d\n", n_lines);
  for (int k = 0; k < n_lines; k++) {
    printf("line %d\n", k+1);
    puts( *(arr+k) );
  }

  return 0;
}
