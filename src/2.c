#include <stdio.h>
#include <stdlib.h>

#define MAX_STRING_SIZE 4
#define MAX_NUMBER_STRINGS 2

int
main(void) {
  FILE *f = fopen("file.txt", "r");
  char (*strings_p)[MAX_NUMBER_STRINGS][MAX_STRING_SIZE] =
    malloc(MAX_NUMBER_STRINGS*MAX_STRING_SIZE*sizeof(char));
  //equivalently
  //calloc(MAX_NUMBER_STRINGS, MAX_STRING_SIZE*sizeof(char));

  for (char (*p)[MAX_STRING_SIZE] = *strings_p;
       p < *strings_p+MAX_NUMBER_STRINGS && fgets(*p, MAX_STRING_SIZE, f);
       p++)
    ; // as linhas ficaram com o \n


  for (int k = 0; k < MAX_NUMBER_STRINGS; k++)
    printf("%s", (*strings_p)[k]);

  fclose(f);
  return 0;
}
