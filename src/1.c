#include <stdio.h>

#define MAX_STRING_SIZE 4
#define MAX_NUMBER_STRINGS 2

int
main(void) {
  FILE *f = fopen("file.txt", "r");
  char strings[MAX_NUMBER_STRINGS][MAX_STRING_SIZE];

  for (char (*p)[MAX_STRING_SIZE] = strings;
       p < strings+MAX_NUMBER_STRINGS && fgets(*p, MAX_STRING_SIZE, f);
       p++)
    ; // as linhas ficaram com o \n


  for (int k = 0; k < MAX_NUMBER_STRINGS; k++)
    printf("%s", strings[k]);

  fclose(f);
  return 0;
}
