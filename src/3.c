#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUF_SIZE 4
#define MAX_NUMBER_STRINGS 2

int
main(void) {
  FILE *f = fopen("file.txt", "r");

  char *strings[MAX_NUMBER_STRINGS],
    buf[BUF_SIZE];

  for (char* *p = strings;
       p < strings+MAX_NUMBER_STRINGS && fgets(buf, BUF_SIZE, f);
       p++) {
    *p = malloc( (1+strlen(buf))*sizeof(char));
    if (*p == NULL) return 1;
    strcpy(*p, buf);
  }

  for (char* *p = strings; p < strings+MAX_NUMBER_STRINGS; p++)
    printf("%s", *p);

  fclose(f);
  return 0;
}
